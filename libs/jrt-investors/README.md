# jrt-investors

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test jrt-investors` to execute the unit tests via [Jest](https://jestjs.io).
