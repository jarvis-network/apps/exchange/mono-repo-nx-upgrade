module.exports = {
  skipFiles: [
    '@chainlink',
    '@openzeppelin',
    '@uma',
    'contracts/test',
    'contracts/extensions',
  ],
};
